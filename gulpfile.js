'use strict';

var gulp = require('gulp'),
  watch = require('gulp-watch'),
  prefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  rigger = require('gulp-rigger'),
  cssmin = require('gulp-minify-css'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  rimraf = require('rimraf'),
  browserSync = require('browser-sync'),
  spritesmith = require('gulp.spritesmith'),
  buffer = require('vinyl-buffer'),
  babel = require('gulp-babel'),
  // wait = require('gulp-wait'),

  // POSTCSS
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  // ERROR HANDLE
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify'),
  reload = browserSync.reload;

var processors = [autoprefixer({ browsers: ['last 3 version'] })];

var plumberErrorHandler = {
  // errorHandler: notify.onError({
  //   title: 'Gulp',

  //   message: 'Error: <%= error.message %>'
  // })
};

var path = {
  build: {
    html: 'build/pages/',
    js: 'build/stuff/js/',
    css: 'build/stuff/css/',
    img: 'build/stuff/img/',
    sprites: 'build/stuff/sprites/',
    fonts: 'build/stuff/fonts/'
  },
  src: {
    html: 'src/*.html',
    js: 'src/js/**/*.js',
    style: 'src/scss/style.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*',
    sprites: 'src/sprites/**/*.*'
  },
  watch: {
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/scss/**/*.scss',
    sprites: 'src/sprites/**/*.*',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: './build'
};

var config = {
  server: {
    baseDir: './build'
  },
  // tunnel: true,
  host: 'localhost',
  port: 8000,
  logPrefix: 'Lipa'
};

gulp.task('html:build', function() {
  gulp
    .src(path.src.html) //Выберем файлы по нужному пути
    .pipe(rigger()) //Прогоним через rigger
    .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
    .pipe(reload({ stream: true })); //И перезагрузим наш сервер для обновлений
});

gulp.task('style:build', function() {
  gulp
    .src(path.src.style) //Выберем наш main.scss
    // .pipe(wait(1500))
    // .pipe(sourcemaps.init()) //То же самое что и с js

    .pipe(plumber(plumberErrorHandler))
    .pipe(sass())
    .pipe(postcss(processors)) //запуск постCss
    // .pipe(prefixer()) //Добавим вендорные префиксы
    .pipe(cssmin()) //Сожмем
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css)) //И в build

    .pipe(reload({ stream: true }));
});

gulp.task('js:build', function() {
  gulp
    .src(path.src.js) //Найдем наш main файл
    .pipe(plumber(plumberErrorHandler))
    .pipe(rigger()) //Прогоним через rigger
    // .pipe(babel())
    // .pipe(sourcemaps.init()) //Инициализируем sourcemap
    // .pipe(uglify()) //Сожмем наш js
    // .pipe(sourcemaps.write()) //Пропишем карты
    .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
    .pipe(reload({ stream: true })); //И перезагрузим сервер
});

gulp.task('sprite:build', function() {
  var spriteData = gulp.src(path.src.sprites).pipe(
    spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.scss',
      padding: 2
    })
  );
  var imgStream = spriteData.img.pipe(gulp.dest(path.build.sprites));

  // Pipe CSS stream through CSS optimizer and onto disk
  var cssStream = spriteData.css.pipe(gulp.dest('src/scss/'));
});

gulp.task('image:build', function() {
  gulp
    .src(path.src.img) //Выберем наши картинки
    .pipe(plumber(plumberErrorHandler))
    // .pipe(imagemin({ //Сожмем их
    //     progressive: true,
    //     svgoPlugins: [{removeViewBox: false}],
    //     use: [pngquant()],
    //     interlaced: true
    // }))
    .pipe(gulp.dest(path.build.img)) //И бросим в build
    .pipe(reload({ stream: true }));
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts).pipe(gulp.dest(path.build.fonts));
});

gulp.task('build', ['html:build', 'js:build', 'style:build', 'fonts:build', 'image:build', 'sprite:build']);

gulp.task('watch', function() {
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.sprites], function(event, cb) {
    gulp.start('sprite:build');
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('webserver', function() {
  browserSync(config);
});

gulp.task('clean', function(cb) {
  rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);

// gulp.task('default', ['build', 'watch']);
