$(function() {
  $(window).on('load', function() {
    $('.main-logo').css({
      opacity: '1',
      transform: 'scale(1)'
    });
  });
  let menuIsClicked = false;
  $('.dropdown').click(event => {
    if ($('.sub-menu').height()) {
      $('.sub-menu').css({
        height: `0`
      });
    } else {
      let height = 0;
      $('.menu-item').each(function() {
        const el = $(this)
          .not('.active')
          .outerHeight();
        height += el ? el : 0;
      });
      $('.sub-menu').css({
        height: `${height}px`
      });
    }
  });
  $('.search').click(() => {
    $('.search-form').addClass('show-form');
  });
  $('.search-close').click(() => {
    $('.search-form').removeClass('show-form');
  });
  $('#hamburger-3').click(function() {
    $(this).toggleClass('is-active');
    $('.header-menu')
      .find('.main-list')
      .toggleClass('show');
  });
  $('#hamburger-1').click(function() {
    $(this).toggleClass('is-active');
    closeList('#hamburger-2', '.second-list-title');
    $('.first-list-title').toggleClass('show');
  });
  $('#hamburger-2').click(function() {
    $(this).toggleClass('is-active');
    closeList('#hamburger-1', '.first-list-title');
    $('.second-list-title').toggleClass('show');
  });
  function closeList(idH, list) {
    $(idH).removeClass('is-active');
    $(list).removeClass('show');
  }
  $('.main-list')
    .find('.arrow')
    .each(function(index) {
      $(this).click(function() {
        menuIsClicked = true;
        $(`#second-list-${index + 1}`).toggleClass('show-list');
        $(this).toggleClass('rotate');
      });
    });

  $('.tab').click(function() {
    var idClicked = $(this)[0].id,
      parent = $(this).parent()[0].id;
    $('.tab').each(function(index) {
      var current = $(this);
      var parentCurr = current.parent()[0].id;
      if (parent === parentCurr && current.hasClass('tab-active')) {
        current.removeClass('tab-active');
      }
    });
    $(this).toggleClass('tab-active');
    $('.slide').each(function() {
      var current = $(this);
      var currentPar = current.parent().siblings()[0].id;
      if (parent === currentPar && current.hasClass('slide-show')) {
        current.removeClass('slide-show');
      }
    });
    $(`#slide-${idClicked.substr(idClicked.length - 1)}`).toggleClass('slide-show');
  });

  $(window).resize(function() {
    if (window.innerWidth > 1070 && menuIsClicked) {
      $('.arrow').each(function() {
        if ($(this).hasClass('rotate')) {
          $(this).removeClass('rotate');
        }
      });
      $('.second-list').each(function() {
        if ($(this).hasClass('show-list')) {
          $(this).removeClass('show-list');
          menuIsClicked = false;
        }
      });
    }
  });
});
