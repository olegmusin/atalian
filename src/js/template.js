$(function() {
  $('.spoiler').each(function(index) {
    $(`#spoiler-${index + 1}`).click(function() {
      const spoilerMain = $(this);
      let spoiler = spoilerMain.find('.spoiler-inner');
      spoiler.slideToggle(function() {
        spoilerMain.find('i').toggleClass('rotate');
      });
    });
  });
});
